<?php
class sendinBlue{
	const API_SENDINBLUE='https://api.sendinblue.com/v3/smtp/email';
	private $user=NULL;
	private $key=NULL;
	private $error=NULL;
	private $error_code=NULL;
	private $sucess=NULL;
	private $sucess_code=NULL;
	private $email_from=NULL;
	private $email_from_name=NULL;
	private $email_to=NULL;
	private $email_to_name=NULL;
	private $subject=NULL;
	private $attached=NULL;
	private $htmlBody=NULL;

	/**
	* establece el correo From
	*
	* @param string $a mail from
	* @param string $n mail from name person
	*/
	public function setMailFrom($a=NULL, $n=NULL) {
		$this->email_from= ($a ? $a:NULL);
		$this->email_from_name= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo From
	*
	* @param string $a mail from name person
	*/
	public function setMailFromName($a=NULL) {
		$this->email_from_name= ($a ? $a:NULL);
	}

	/**
	* establece el correo To
	*
	* @param string $a mail to
	* @param string $n mail to name person
	*/
	public function setMailTo($a=NULL, $n=NULL) {
		$this->email_to= ($a ? $a:NULL);
		$this->email_to_name= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo To
	*
	* @param string $a mail to name person
	*/
	public function setMailToName($a=NULL) {
		$this->email_to_name= ($a ? $a:NULL);
	}

	/**
	* establece el asunto
	*
	* @param string $a mail subject
	*/
	public function setSubject($a=NULL) {
		$this->subject= ($a ? $a:NULL);
	}

	/**
	* establece el contenido en html del cuerpo
	*
	* @param string $a mail html body content
	*/
	public function setHtmlContent($a=NULL) {
		$this->htmlBody= ($a ? $a:NULL);
	}

	/**
	* retorna el mail from
	*
	* @return string mail from
	*/
	public function getMailFrom() {
		return $this->email_from;
	}

	/**
	* retorna el nombre del from
	*
	* @return string name from
	*/
	public function getMailFromName() {
		return $this->email_from_name;
	}

	/**
	* retorna el mail to
	*
	* @return string mail to
	*/
	public function getMailTo() {
		return $this->email_to;
	}

	/**
	* retorna el nombre del to
	*
	* @return string name to
	*/
	public function getMailToName() {
		return $this->email_to_name;
	}

	/**
	* retorna el mail subject
	*
	* @return string mail subject
	*/
	public function getSubject() {
		return $this->subject;
	}

	/**
	* retorna el mail html content
	*
	* @return string mail html content
	*/
	public function getHtmlContent() {
		return $this->htmlBody;
	}

	/**
	* establece el mensaje de error
	*
	* @param string $a el mensaje de error
	* @param string $c el codigo de error
	*/
	public function setError($a=NULL, $c=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->error_code= ($c ? $c:NULL);
	}

	/**
	* retorna el mensaje de error
	*
	* @return string el mensaje de error con codigo
	*/
	public function getError() {
		return ($this->error_code ? $this->error_code. '-':'').$this->error;
	}

	/**
	* retorna el codigo de error
	*
	* @return string el codigo de error
	*/
	public function getErrorCode() {
		return $this->error_code;
	}

	/**
	* establece el mensaje de exito
	*
	* @param string $a el mensaje de exito
	* @param string $c el codigo de exito
	*/
	public function setSucess($a=NULL, $c=NULL) {
		$this->sucess= ($a ? $a:NULL);
		$this->sucess_code= ($c ? $c:NULL);
	}

	/**
	* retorna el mensaje de exito
	*
	* @return string el mensaje de exito con codigo
	*/
	public function getSucess() {
		return ($this->sucess_code ? $this->sucess_code. '-':'').$this->sucess;
	}

	/**
	* retorna el codigo de exito
	*
	* @return string el codigo de exito
	*/
	public function getSucessCode() {
		return $this->sucess_code;
	}

	/**
	* retorna el mensaje de exito o response en transacciones
	*
	* @return string el mensaje de exito con codigo
	*/
	public function getResponse() {
		return ($this->sucess_code ? $this->sucess_code. '-':'').$this->sucess;
	}

	/**
	* procesa el envio y llama el debugeo
	*/
	public function send() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
		else if( !$this->subject )	$this->setError("no indico el asunto del correo", "003");
		else if( !$this->email_from )	$this->setError("no indico el correo del emisor", "004");
		else if( !$this->email_to )	$this->setError("no indico el correo del receptor", "005");
		else if( !$this->htmlBody )	$this->setError("no indico el contenido o cuerpo del correo", "006");
		else {
			return 1;
		}
	}

	/**
	* envia la informacion al api sendinblue.com
	*/
	public function sendToSendinblue() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
	}

	/**
	* establecemos el usuario o correo electronic
	*
	* @param string $a el usuario o correo
	*/
	public function setUser($a=NULL) {
		$this->user= ($a ? $a:NULL);
	}

	/**
	* establecemos la clave del api
	*
	* @param string $a la clave del token del api
	*/
	public function setKey($a=NULL) {
		$this->key= ($a ? $a:NULL);
	}

	/**
	* inicializa los dato para conexiones futuras
	*
	* @param string $user el usuario o correo
	* @param string $key el token del key api
	*/
	public function __construct($user=NULL, $key=NULL) {
		$this->setUser($user);
		$this->setKey($key);
	}
}
?>